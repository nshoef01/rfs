public class JavaStringCalculator {

    //do not except negative numbers

    public String calculate(String expression) {
        boolean valid = validate(expression);
        if (!valid) return "Invalid expression";
        return String.valueOf(getValue(expression));
    }

    private boolean validate(String expression) {
        return expression.matches("\\d+(.\\d+)?((\\+|\\*|\\-|\\/)\\d+(.\\d+)?)*") && !expression.matches(".*/0.*");
    }

    private double getValue(String expression) {
        String[] result = expression.split("\\+");
        if(result.length > 1) {
            double r = getValue(result[0]);
            for(int i=1; i< result.length; i++) {
                r += Double.valueOf(calculate(result[i]));
            }
            return r;
        }
        result = expression.split("-");
        if(result.length > 1) {
            double r = getValue(result[0]);
            for(int i=1; i<result.length; i++) {
                r -= Double.valueOf(calculate(result[i]));
            }
            return r;
        }

        result = expression.split("/");
        if(result.length > 1) {
            double r = getValue(result[0]);
            for(int i=1; i< result.length; i++) {
                r /= Double.valueOf(calculate(result[i]));
            }
            return r;
        }

        result = expression.split("\\*");
        if(result.length > 1) {
            double r = getValue(result[0]);
            for(int i=1; i< result.length; i++) {
                r *= Double.valueOf(calculate(result[i]));
            }
            return r;
        }

        return Double.valueOf(expression);
    }
}
