import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class StringCalculatorTest {

    //private JavaStringCalculator calculator;
    private ScalaStringCalculator calculator;


    @BeforeEach
    public void initTest() {
        //calculator = new JavaStringCalculator();
        calculator = new ScalaStringCalculator();
    }


    @Test
    public void shouldReturnErrorIfExpressionIsInvalid() {
        assertEquals("Invalid expression", calculator.calculate("4rt56+45"));
        assertEquals("Invalid expression", calculator.calculate("+45-7.0"));
        assertEquals("Invalid expression", calculator.calculate("5-*7.0"));
        assertEquals("Invalid expression", calculator.calculate("+45-7.0"));
        assertEquals("Invalid expression", calculator.calculate("456+"));
        assertEquals("Invalid expression", calculator.calculate("3/0"));
    }


    @Test
    public void shouldAdd2Number() {
        assertEquals("2.0", calculator.calculate("1+1"));
        assertEquals("4.0", calculator.calculate("3+1"));
    }

    @Test
    public void shouldAddMoreThan2Number() {
        assertEquals("6.0", calculator.calculate("3+1+2"));
        assertEquals("10.0", calculator.calculate("3+1+2+4.0"));
    }

    @Test
    public void shouldSubtractNumbers() {
        assertEquals("2.0", calculator.calculate("3-1"));
        assertEquals("0.5", calculator.calculate("6-4-1.5"));
        assertEquals("-4.0", calculator.calculate("2-4.0-2"));
    }


    @Test
    public void shouldMultiplyNumbers() {
        assertEquals("4.0", calculator.calculate("2*2"));
        assertEquals("12.0", calculator.calculate("1*6*2"));
    }

    @Test
    public void shouldDivideNumbers() {
        assertEquals("2.0", calculator.calculate("4/2"));
        assertEquals("1.6", calculator.calculate("3.2/2"));
        assertEquals("0.0", calculator.calculate("0/2"));
    }

    @Test
    public void shouldEnableMultiOperations() {
        assertEquals("11.5", calculator.calculate("2.5+3*3"));
        assertEquals("0.5", calculator.calculate("3/2-1"));
    }
}
