class ScalaStringCalculator {


  def calculate(string: String): String = {
    if (string.matches("\\d+(.\\d+)?((\\+|\\*|\\-|\\/)\\d+(.\\d+)?)*") && !string.matches(".*/0.*")) {
      return eval(string)
    }
    return "Invalid expression"
  }


  def eval(exp: String) : String = {
    val multi = """(.*)(\*)(.*)""".r;
    val divide = """(.*)(\/)(.*)""".r;
    val add = """(.*)(\+)(.*)""".r;
    val subtract = """(.*)(\-)(.*)""".r;

    exp match {
      case add(a1, op, a2) => (eval(a1).toDouble + eval(a2).toDouble).toString()
      case subtract(a1, op, a2) => (eval(a1).toDouble - eval(a2).toDouble).toString()
      case multi(a1, op, a2) => (eval(a1).toDouble * eval(a2).toDouble).toString()
      case divide(a1, op, a2) => (eval(a1).toDouble / eval(a2).toDouble).toString()
      case _ => exp
    }
  }
}